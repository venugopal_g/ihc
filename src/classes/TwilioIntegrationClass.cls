public class TwilioIntegrationClass {

    public static TwilioResponseWrap sendTwilioSMS(String toNum, String smsBody){
        
        Twilio_Config_Custom__c tcs     = Twilio_Config_Custom__c.getInstance();
        Blob headerValue                = Blob.valueOf(tcs.AccountSid__c +':' +tcs.AuthToken__c);
        String authorizationHeader      = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        String body                     = 'From='+tcs.From_Number__c+'&To='+toNum+'&Body='+smsBody;
        String url = tcs.Endpoint_URL__c;
        HttpRequest req = new HttpRequest(); 
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('Authorization', authorizationHeader); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        
        req.setBody(body);
        Http http = new Http();
        HTTPResponse res;
        try{
            res = http.send(req);  
            system.debug('res::::'+res);
            system.debug('res::::'+res.getbody());
        }catch(Exception e){
            return null;
        }
        
        return null;
    }
}