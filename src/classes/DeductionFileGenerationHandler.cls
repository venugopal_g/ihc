public class DeductionFileGenerationHandler{

public static void generateFileForEmp(Set<Id> empIds,date dueDate, loan.TransactionSweepToACHState state){
    
          //  List<String> fileType = new List<String>();         
          //  fileType.add('CSV');
           // fileType.add('TEXT');
            List<String> supportedObjectType = new List<String>();
            //add the loan__Loan_account_Due_Details__c object first so we can prepare the bill query first then the relationship query
            supportedObjectType.add('loan__Loan_account_Due_Details__c');
            supportedObjectType.add('loan__Loan_Account__c');
            supportedObjectType.add('Account');
            supportedObjectType.add('Contact');
            supportedObjectType.add('genesis__Employment_Information__c');
            
           
            Map<id,account> accMap = new Map<id,account>();
            Map<String,Set<String>> objectToFieldNames = new Map<String,Set<String>>();     
            Map<Id,List<Deduction_File_Configuration__c>> dfcsToDeductConfig= new Map<Id,List<Deduction_File_Configuration__c>>();
            Map<Id,Id> accToDeductConfigSet= new Map<Id,Id>();
            Set<Id> dfcsIds = new Set<Id>();
            Map<String,Set<Id>> accWithFileType = new Map<String,Set<ID>>();
            Set<Id> contactIds= new Set<Id>();
            
            List<account> acclst=[Select id,Name,Generate_Different_Deduction_Files__c,Deduction_File_Type__c,(Select id,Account__c,Deduction_File_Configurations_Set__c from Acc_To_DFC_Set_Junction__r limit 1) from account where id IN:empIds];
            for(account acc: acclst){
               accMap.put(acc.id,acc);             
            
               for(Acc_To_DFC_Set_Junction__c dfJun: acc.Acc_To_DFC_Set_Junction__r){                  
                   dfcsIds.add(dfJun.Deduction_File_Configurations_Set__c);
                   accToDeductConfigSet.put(acc.id,dfJun.Deduction_File_Configurations_Set__c);
                }
             /*  List<Deduction_File_Configuration__c> tempDeductFile = new List<Deduction_File_Configuration__c>();
               for(Deduction_File_Configuration__c df: acc.Deduction_File_Configurations__r){
                   
                   tempDeductFile.add(df);
                   if(!objectToFieldNames.containsKey(df.Mapped_Object_Name__c)){
                       
                       objectToFieldNames.put(df.Mapped_Object_Name__c,new Set<String>{df.Mapped_Field_API_Name__c});
                   }
                   else{
                       
                       objectToFieldNames.get(df.Mapped_Object_Name__c).add(df.Mapped_Field_API_Name__c);
                   }
                   
               }
               accToDeductConfig.put(acc.id,tempDeductFile);              
               */
               string fileType='CSV';
               if(acc.Deduction_File_Type__c=='TEXT'){
                   fileType='TEXT';                
               }   
               if(!accWithFileType.containsKey(fileType)){
                   
                   accWithFileType.put(fileType,new Set<Id>{acc.id});
               }
               else{                   
                   accWithFileType.get(fileType).add(acc.id);                  
               }
               
            }
            
            List<Deduction_File_Configurations_Set__c> dfcSetlst = new List<Deduction_File_Configurations_Set__c>();
            dfcSetlst=[select id,(Select id,Date_Format__c,Field_Type__c,Header_Label__c,Mapped_Field_API_Name__c,Mapped_Object_Name__c,Sequence__c from Deduction_File_Configurations__r order by Sequence__c) from Deduction_File_Configurations_Set__c where id IN:dfcsIds];
           
            for(Deduction_File_Configurations_Set__c dfsc: dfcSetlst){
                
                  List<Deduction_File_Configuration__c> tempDeductFile = new List<Deduction_File_Configuration__c>();
                  for(Deduction_File_Configuration__c df:dfsc.Deduction_File_Configurations__r){                      
                       
                         tempDeductFile.add(df);
                       if(!objectToFieldNames.containsKey(df.Mapped_Object_Name__c)){
                           
                           objectToFieldNames.put(df.Mapped_Object_Name__c,new Set<String>{df.Mapped_Field_API_Name__c});
                       }
                       else{
                           
                           objectToFieldNames.get(df.Mapped_Object_Name__c).add(df.Mapped_Field_API_Name__c);
                       }
                   
                    }
                dfcsToDeductConfig.put(dfsc.id,tempDeductFile);
                
             }
           
            //Create query string for all objects
            String billQuery='Select id,loan__Loan_Account__r.loan__Contact__c,loan__Loan_Account__r.loan__Account__c,loan__Loan_Account__r.loan__Frequency_of_Loan_Payment__c';
            String empQuery;
            for(string objectName : supportedObjectType){
                
                  if(objectName=='loan__Loan_account_Due_Details__c' && objectToFieldNames.containsKey(objectName) && objectToFieldNames.get(objectName).size()>0){
                       for(String s: objectToFieldNames.get(objectName)){
                              if(s!='Id'){
                                 billQuery+=','+s;
                              }
                          }
                     
                  }
                  else if(objectName=='loan__Loan_Account__c' && objectToFieldNames.containsKey(objectName) && objectToFieldNames.get(objectName).size()>0){                      
                          //String loanAccQuery='loan__Loan_Account__c,';
                          String loanAccQuery='';
                          for(String s: objectToFieldNames.get(objectName)){
                                if(s!='loan__Contact__c' && s!='loan__Account__c' && s!='loan__Frequency_of_Loan_Payment__c'){                              
                                    loanAccQuery+='loan__Loan_Account__r.'+s+','; 
                                }                                
                          }
                          loanAccQuery=loanAccQuery.removeEnd(',');
                          billQuery=billQuery+','+loanAccQuery;
                          
                        
                  }
                  else if(objectName=='Account' && objectToFieldNames.containsKey(objectName) && objectToFieldNames.get(objectName).size()>0){                    
                          String accQuery='';                        
                          for(String s: objectToFieldNames.get(objectName)){                              
                                 accQuery+='loan__Loan_Account__r.loan__Account__r.'+s+',';                           
                          }
                          accQuery=accQuery.removeEnd(',');
                          billQuery=billQuery+','+accQuery;
                          
                        
                  }
                  else if(objectName=='Contact' && objectToFieldNames.containsKey(objectName) && objectToFieldNames.get(objectName).size()>0){                    
                          String contactQuery='';                        
                          for(String s: objectToFieldNames.get(objectName)){                              
                                 contactQuery+='loan__Loan_Account__r.loan__Contact__r.'+s+',';                           
                          }
                          contactQuery=contactQuery.removeEnd(',');
                          billQuery=billQuery+','+contactQuery;                       
                        
                  }
                  else if(objectName=='genesis__Employment_Information__c' && objectToFieldNames.containsKey(objectName) && objectToFieldNames.get(objectName).size()>0){
                      
                        empQuery='Select Id,genesis__Contact__c,';
                         for(String s: objectToFieldNames.get(objectName)){                           
                                if(s!='Id' && s!='genesis__Contact__c'){                                
                                empQuery+=s+',';
                               }                                
                          }
                         empQuery=empQuery.removeEnd(',');
                         empQuery+=' from genesis__Employment_Information__c where genesis__Contact__c IN:contactIds';
                      
                  }       
                  
                
            }
            
            loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();
            Date todaySystemDate=info.getCurrentSystemDate(); 
            
            Date dueGenerationDate;
            
            if(dueDate!=null){
                dueGenerationDate=dueDate;              
            }
            else{
                
                dueGenerationDate=todaySystemDate;
            }
            
            billQuery+=' from loan__Loan_account_Due_Details__c where loan__Loan_Account__r.loan__Account__c IN:empIds and loan__Due_Date__c=:dueGenerationDate and loan__Balance_Amount__c >0 and loan__Payment_Satisfied__c=false and loan__DD_Primary_Flag__c=true';
            system.debug('****billQuery'+billQuery);
            List<loan__Loan_account_Due_Details__c> allBills=database.query(billQuery);
            
            Map<Id,List<loan__Loan_account_Due_Details__c>> accToBills = new Map<Id,List<loan__Loan_account_Due_Details__c>>();
            Map<Id,Map<String,List<loan__Loan_account_Due_Details__c>>> accToPayCyleWithBills = new Map<Id,Map<String,List<loan__Loan_account_Due_Details__c>>>();
            
            for(loan__Loan_account_Due_Details__c loanDue : allBills){
                                
                        if(loanDue.loan__Loan_Account__r.loan__Account__c!=null && accMap.containsKey(loanDue.loan__Loan_Account__r.loan__Account__c)){
                            
                             if(accMap.get(loanDue.loan__Loan_Account__r.loan__Account__c).Generate_Different_Deduction_Files__c==true){
                                 
                                  if(!accToPayCyleWithBills.containsKey(loanDue.loan__Loan_Account__r.loan__Account__c)){
                                      
                                      Map<String,List<loan__Loan_account_Due_Details__c>> tempAccBillsMap = new Map<String,List<loan__Loan_account_Due_Details__c>>();
                                      tempAccBillsMap.put(loanDue.loan__Loan_Account__r.loan__Frequency_of_Loan_Payment__c,new List<loan__Loan_account_Due_Details__c>{loanDue});                                      
                                      accToPayCyleWithBills.put(loanDue.loan__Loan_Account__r.loan__Account__c,tempAccBillsMap); 
                                      
                                  }
                                  else{
                                      
                                      if(!accToPayCyleWithBills.get(loanDue.loan__Loan_Account__r.loan__Account__c).containsKey(loanDue.loan__Loan_Account__r.loan__Frequency_of_Loan_Payment__c)){
                                          
                                          accToPayCyleWithBills.get(loanDue.loan__Loan_Account__r.loan__Account__c).put(loanDue.loan__Loan_Account__r.loan__Frequency_of_Loan_Payment__c,new List<loan__Loan_account_Due_Details__c>{loanDue});
                                      }
                                      else{
                                          
                                          accToPayCyleWithBills.get(loanDue.loan__Loan_Account__r.loan__Account__c).get(loanDue.loan__Loan_Account__r.loan__Frequency_of_Loan_Payment__c).add(loanDue);
                                      }
                                      
                                  }
                                 
                             }
                             else{
                                                                 
                                 if(!accToBills.containsKey(loanDue.loan__Loan_Account__r.loan__Account__c)){
                                     
                                     accToBills.put(loanDue.loan__Loan_Account__r.loan__Account__c,new List<loan__Loan_account_Due_Details__c>{loanDue});
                                 }
                                 else{
                                     
                                     accToBills.get(loanDue.loan__Loan_Account__r.loan__Account__c).add(loanDue);
                                 }
                                 
                             }
                            
                        }

                    if(empQuery!=null && loanDue.loan__Loan_Account__r.loan__Contact__c!=null){
                        
                        contactIds.add(loanDue.loan__Loan_Account__r.loan__Contact__c);
                    }                       
                
            }
            
             Map<Id,genesis__Employment_Information__c> contactToEmployeeData = new Map<Id,genesis__Employment_Information__c>();
            if(contactIds.size()>0){                
              system.debug('***empQuery'+empQuery);
              system.debug('***contactIds'+contactIds);
                for(genesis__Employment_Information__c emp :database.query(empQuery)){
                    
                    contactToEmployeeData.put(emp.genesis__Contact__c,emp);
                }
            }
            
            loan__ACH_Parameters__c achParam = loan.CustomSettingsUtil.getACHParameters();
            boolean textUpdated=false;          
            if(accWithFileType.ContainsKey('TEXT')){
                
                 if(achParam.loan__ACH_File_Type__c!='TEXT'){
                     achParam.loan__ACH_File_Type__c='TEXT';
                     update achParam;
                     textUpdated=true;
                     
                 }
                 
                 for(Id accId : accWithFileType.get('TEXT')){
                     
                      List<Deduction_File_Configuration__c> dfLst = new List<Deduction_File_Configuration__c>();
                      if(accToDeductConfigSet.get(accId)!=null &&  dfcsToDeductConfig.get(accToDeductConfigSet.get(accId)).size()>0){
                           dfLst=dfcsToDeductConfig.get(accToDeductConfigSet.get(accId));
                      }
                      
                       if(accToPayCyleWithBills.containsKey(accId)){
                           
                           Map<String,List<loan__Loan_account_Due_Details__c>> payCycleMap=accToPayCyleWithBills.get(accId);
                           for(String payCycle: payCycleMap.keySet()){
                            String deductFileName=accMap.get(accId).Name+'-'+payCycle;                          
                            loan.FileGenerator filegen = new DeductionFileGenFromPackage(deductFileName,true,contactToEmployeeData,dfLst);
                            filegen.setScope(payCycleMap.get(payCycle));
                            filegen.createLines(state);
                            String filename = filegen.writeFile(state);
                            system.debug('filename:::::::::::::::::'+filename);
                            //List<SObject> validScope = filegen.getValidScope(); 
                           }
                           
                       }
                       if(accToBills.ContainsKey(accId)){
                           
                            loan.FileGenerator filegen = new DeductionFileGenFromPackage(accMap.get(accId).Name,true,contactToEmployeeData,dfLst);
                            filegen.setScope(accToBills.get(accId));
                            filegen.createLines(state);
                            String filename = filegen.writeFile(state);
                            system.debug('filename:::::::::::::::::'+filename);
                            //List<SObject> validScope = filegen.getValidScope(); 
                           
                       }                        
                     
                     
                 }               
                
                
            }
            
            if(accWithFileType.ContainsKey('CSV')){             
                
                 if(textUpdated || achParam.loan__ACH_File_Type__c!='CSV'){
                     achParam.loan__ACH_File_Type__c='CSV';
                     update achParam;                
                 }
                 
                for(Id accId : accWithFileType.get('CSV')){ 
                
                   
                  List<Deduction_File_Configuration__c> dfLst = new List<Deduction_File_Configuration__c>();
                  if(accToDeductConfigSet.get(accId)!=null &&  dfcsToDeductConfig.get(accToDeductConfigSet.get(accId)).size()>0){
                       dfLst=dfcsToDeductConfig.get(accToDeductConfigSet.get(accId));
                  }
                  
                  if(dfLst.size()>0){
                      
                    if(accToPayCyleWithBills.containsKey(accId)){

                            Map<String,List<loan__Loan_account_Due_Details__c>> payCycleMap=accToPayCyleWithBills.get(accId);
                            for(String payCycle: payCycleMap.keySet()){
                            String deductFileName=accMap.get(accId).Name+'-'+payCycle;
                            loan.FileGenerator filegen = new DeductionFileGenFromPackage(deductFileName,false,contactToEmployeeData,dfLst);
                            filegen.setScope(payCycleMap.get(payCycle));
                            filegen.createLines(state);
                            String filename = filegen.writeFile(state);
                            system.debug('filename:::::::::::::::::'+filename);
                            //List<SObject> validScope = filegen.getValidScope(); 
                         }
                    
                    }
                    if(accToBills.ContainsKey(accId)){

                        loan.FileGenerator filegen = new DeductionFileGenFromPackage(accMap.get(accId).Name,false,contactToEmployeeData,dfLst);
                        filegen.setScope(accToBills.get(accId));
                        filegen.createLines(state);
                        String filename = filegen.writeFile(state);
                        system.debug('filename:::::::::::::::::'+filename);
                        //List<SObject> validScope = filegen.getValidScope(); 

                    }
                  } 
                }               
                
            }   
    
    }


}