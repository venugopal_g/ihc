public class LoanDocumentController {
    public String contractId{get;set;}
    public loan__Loan_Account__c loanDetailes{get;set;}
    public List<loan__Repayment_Schedule__c> payments{get;set;} 
    public LoanDocumentController(apexPages.StandardController controller){
        contractId = ApexPages.currentPage().getParameters().get('Id');
        
        loanDetailes = [Select Name,loan__Contact__r.Name,loan__Contact__r.HomePhone,Loan_Purpose__c,loan__Pmt_Amt_Cur__c,loan__Disbursal_Date__c,loan__First_Installment_Date__c,loan__Last_Installment_Date__c,loan__Frequency_of_Loan_Payment__c,EmployeeAddress__c,Total_Amount_of_Repayments__c,loan__Loan_Amount__c,loan__Number_of_Installments__c,loan__Contractual_Interest_Rate__c,loan__Disbursal_Amount__c,
                        loan__Payment_Amount__c,(select loan__Balance__c,DayOfDate__c,YearOfDate__c,Number__c,loan__Total_Installment__c,loan__Due_Date__c from loan__Repayment_Schedule__r)loan__First_Installment_Date__c,loan__Disbursed_Amount__c from loan__Loan_Account__c where Id=:contractId];
        System.debug('&&&&&&&&&&&&&*********'+loanDetailes);
        payments=new List<loan__Repayment_Schedule__c>();
        for(loan__Repayment_Schedule__c rs:loanDetailes.loan__Repayment_Schedule__r){
            //Date mydate = date.newInstance(rs.loan__Due_Date__c.Year(),rs.loan__Due_Date__c.Month(),rs.loan__Due_Date__c.Day());
            //rs.loan__Due_Date__c = Datevalue(mydate.format());
           payments.add(rs) ;
        }
        
    }   
    
    
}