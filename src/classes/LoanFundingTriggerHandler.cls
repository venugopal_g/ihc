public class LoanFundingTriggerHandler {
   @future(callout=true)
    public static void SendSms(Set<Id> ldtIds){
        for(loan__Loan_Disbursal_Transaction__c ltxn:[Select id,loan__Disbursed_Amt__c,loan__Loan_Account__r.loan__Contact__r.MobilePhone from loan__Loan_Disbursal_Transaction__c where Id In:ldtIds]){
            String SMS = 'Congratulations! Your loan of'+ltxn.loan__Disbursed_Amt__c+' has been funded to your bank account.'
                +' The amount will appear in <no of days>.'
                +' To make a payment outside of your automatic payroll deduction, log-in to <In-house capital link for “Make payment”>';
                
              TwilioIntegrationClass.sendTwilioSMS(ltxn.loan__Loan_Account__r.loan__Contact__r.MobilePhone,SMS);
               system.debug('Sms sendin...');
        }
    }
}