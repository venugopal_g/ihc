public class TriggerHandlerForCreatePricing {
    public static void CreatingPricing(List<genesis__Applications__c> appList){
       // genesis__Application_Pricing_Detail__c[] createPricing;
        List<genesis__Application_Pricing_Detail__c> createPricing = new List<genesis__Application_Pricing_Detail__c>();
      //  appList=[select id,genesis__Interest_Rate__c,genesis__Payment_Frequency__c,genesis__Loan_Amount__c,genesis__Term__c from genesis__Applications__c  limit 1];
       system.debug('+++++++++++++++'+appList);
        for(genesis__Applications__c app:appList){
            genesis__Application_Pricing_Detail__c pric= new genesis__Application_Pricing_Detail__c();
                pric.genesis__Application__c=app.id;
                pric.genesis__Interest_Rate__c=app.genesis__Interest_Rate__c;
                pric.genesis__Payment_Frequency__c=app.genesis__Payment_Frequency__c;
                pric.genesis__Maximum_Financed_Amount__c=app.genesis__Loan_Amount__c -500;
                pric.genesis__Term__c=app.genesis__Term__c;
                pric.genesis__Yield_Percent__c=10;
                pric.genesis__Rate_Factor__c=30;
                pric.genesis__Rate_Factor_Based_On_Amount__c=200;
                createPricing.add(pric);
                system.debug('====================='+createPricing);
            }
           
        
         try{
                insert createPricing;
            }
            catch (Exception Ex){
				system.debug(Ex);
			}
    }
    @future(callout=true)
    public static void SendSms(set<id> appids){
        
        for(genesis__Applications__c app:[Select id,Name,genesis__Contact__c,genesis__Contact__r.MobilePhone from genesis__Applications__c where id In:appids]){
            String sms = 'Congratulations! Your In-House Capital personal loan application number '+app.Name+
                ' has been approved. Pls login to your account to upload the documents. Pls ignore if already uploaded.';
           
               TwilioIntegrationClass.sendTwilioSMS(app.genesis__Contact__r.MobilePhone,sms);
            system.debug('Sms sendin...');
        }
    }
}