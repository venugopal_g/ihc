public class TwilioResponseWrap{
	public String sid;	//SM3741bf850b424547a5ae8b0d42578268
	public String date_created;	//Tue, 17 Jan 2017 12:08:49 +0000
	public String date_updated;	//Tue, 17 Jan 2017 12:08:49 +0000
	public cls_date_sent date_sent;
	public String account_sid;	//ACcd63ec68371f22af90eaa2908c4b6c24
	public String to;	//+919611422669
	public String from_t;	//+19042042485
	public String messaging_service_sid;
	public String body;	//Test Email
	public String status;	//queued
	public String num_segments;	//1
	public String num_media;	//0
	public String direction;	//outbound-api
	public String api_version;	//2010-04-01
	public String price;
	public String price_unit;	//USD
	public String error_code;
	public String error_message;
	public String uri;	///2010-04-01/Accounts/ACcd63ec68371f22af90eaa2908c4b6c24/Messages/SM3741bf850b424547a5ae8b0d42578268.json
	public cls_subresource_uris subresource_uris;
	class cls_date_sent {
	}
	class cls_subresource_uris {
		public String media;	///2010-04-01/Accounts/ACcd63ec68371f22af90eaa2908c4b6c24/Messages/SM3741bf850b424547a5ae8b0d42578268/Media.json
	}
	public static TwilioResponseWrap parse(String json){
		return (TwilioResponseWrap) System.JSON.deserialize(json, TwilioResponseWrap.class);
	}
/*
	static testMethod void testParse() {
		String json=		'{'+
		'  "sid": "SM3741bf850b424547a5ae8b0d42578268",'+
		'  "date_created": "Tue, 17 Jan 2017 12:08:49 +0000",'+
		'  "date_updated": "Tue, 17 Jan 2017 12:08:49 +0000",'+
		'  "date_sent": null,'+
		'  "account_sid": "ACcd63ec68371f22af90eaa2908c4b6c24",'+
		'  "to": "+919611422669",'+
		'  "from": "+19042042485",'+
		'  "messaging_service_sid": null,'+
		'  "body": "Test Email",'+
		'  "status": "queued",'+
		'  "num_segments": "1",'+
		'  "num_media": "0",'+
		'  "direction": "outbound-api",'+
		'  "api_version": "2010-04-01",'+
		'  "price": null,'+
		'  "price_unit": "USD",'+
		'  "error_code": null,'+
		'  "error_message": null,'+
		'  "uri": "/2010-04-01/Accounts/ACcd63ec68371f22af90eaa2908c4b6c24/Messages/SM3741bf850b424547a5ae8b0d42578268.json",'+
		'  "subresource_uris": {'+
		'    "media": "/2010-04-01/Accounts/ACcd63ec68371f22af90eaa2908c4b6c24/Messages/SM3741bf850b424547a5ae8b0d42578268/Media.json"'+
		'  }'+
		'}';
		TwillioResponseWrap obj = parse(json);
		System.assert(obj != null);
	}
*/
}