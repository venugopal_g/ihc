global class DeductionFileGeneration implements Schedulable, Database.Batchable<Sobject>{
    String query;   
    loan.TransactionSweepToACHState state;
    Date dueDate;
    
    global DeductionFileGeneration(Date dueDate) {
      
        this.dueDate=dueDate;
        query='Select id,Name,Generate_Different_Deduction_Files__c,Deduction_File_Type__c from account where Type=\'Employer\' and Employer_Agreement_Status__c=\'Active\' ';
        state = new loan.TransactionSweepToACHState();   
    }
    
    global void execute(SchedulableContext sc) {
        DeductionFileGeneration deducFile = new DeductionFileGeneration(null);
        Database.executeBatch(deducFile);
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
    //    system.debug('query111:::::'+query);
        return Database.getQueryLocator(query);
    } // do nothing
    
    global void execute(Database.BatchableContext bc, List<account> accLst) {       
        try {
            
           Set<Id> accIds = new Set<Id>();
           for(account acc: accLst){               
               accIds.add(acc.id);
           }
           if(accIds.size()>0){ 
             DeductionFileGenerationHandler.generateFileForEmp(accIds,dueDate,state);
           }
          
          
        } catch (Exception e) {
            system.debug('Exception:::::::::'+e.getMessage()+''+e.getCause()+''+e.getStackTraceString());
    //        throw new GeneralException(e.getMessage()+' From line '+e.getLinenumber());
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}