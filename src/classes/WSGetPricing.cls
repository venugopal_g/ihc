@RestResource(urlMapping='/getCounterOffer/*')
global class WSGetPricing {
    global class Response{
        public String status; 
        public String errorCode;
        public String errorMessage;
		public string httpStatusCode;
        public Decimal LoanAmount;
        public Decimal PayAmount;
        public Decimal Terms;
        public string CounterOfferID;
        public String PaymentFrequency;
        public Decimal InterestRate;
        public Boolean overallResponse;
        public Response(){
            errorCode = 'NO_ERROR';
            status = 'SUCCESS';
            httpStatusCode = '200';
        }
    }
    @Httpget
    global static Response getCounterOffer(){
    
        RestRequest req = Restcontext.Request;
        RestResponse restRes = Restcontext.response;
        Response res = new Response();
        if(req == null){
            res.errorCode = 'INVALID_INPUT';
            res.status = 'ERROR';
            res.errorMessage = 'UNABLE_PARSE_REQ_PARAMS';
            res.httpStatusCode = '400';
            return res;
        }
         String appId   ;
        try{
            appId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

            system.debug('========='+appId);
        }
        catch(Exception e){
            System.debug('Exception:::::::::'+e);
        }
         if(appId == null){
            res.errorCode      = 'INVALID_INPUT_FORMAT';
            res.status         = 'ERROR';
            res.errorMessage   = 'appId is required.';
            res.httpStatusCode = '400';
            return res;
        }
        if(!(appId instanceOf Id)){
            res.errorCode      = 'INVALID_INPUT_FORMAT';
            res.status         = 'ERROR';
            res.errorMessage   = 'Appid is in incorrect format.';
            res.httpStatusCode = '400';
            return res;
        }
        
        try{
            RulePart rv=new RulePart(appid);
            res.overallResponse=rv.Calcuate();
            system.debug('res.overallResponse'+res.overallResponse);
          // return res;
            if(res.overallResponse==true){
                rv.Equifax(6598, 0, 1156, 1000, 0, 0, appid);
                rv.mit();
                rv.createPricing();
                    
               }
            else{
               res.errorCode='Prebureau failed';
               res.status='Loan Rejected ';
                res.httpStatusCode = '400';
                return res;
            }
        }
            catch(exception e){
               res.errorCode='mit problem catch ';
                 res.status='failed ';
                res.httpStatusCode = '400';
                return res;
            }
        
        genesis__Application_Pricing_Detail__c pric = new genesis__Application_Pricing_Detail__c();
        try{
            pric=[select id,genesis__Application__c,genesis__Interest_Rate__c,genesis__Payment_Frequency__c,genesis__Maximum_Financed_Amount__c,genesis__Term__c,Pay_amount__c from genesis__Application_Pricing_Detail__c where genesis__Application__c=:appid  ORDER BY Createddate desc Limit 1 ];
      
            system.debug('+++++++++++++++++++++'+pric);
            res.status='SUCCESS'; 
           res.errorCode='NO_ERROR';
            
           res.CounterOfferID=pric.Id;
           res.InterestRate=pric.genesis__Interest_Rate__c;
           res.PaymentFrequency=pric.genesis__Payment_Frequency__c;
           res.Terms=pric.genesis__Term__c;
           res.LoanAmount=pric.genesis__Maximum_Financed_Amount__c;
           res.PayAmount=pric.Pay_amount__c;
		   
            
            return res;
        }
        catch(Exception e){
            res.status= 'ERROR';
              res.errorCode='ID_NOT_FOUND ';
              res.errorMessage = e.getMessage();
              res.httpStatusCode = '400';
              return res;
        }
        
        
        
        res.status = 'SUCCESS';
       res.httpStatusCode = '200';
        
        return res;
    
    }
}