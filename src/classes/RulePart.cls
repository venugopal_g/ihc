public class RulePart {
    
  public string ApID;
   public   genesis__Applications__c App;
    public Equifax__c eq;
    public Equifax__c  eq1;
    public Equifax__c  eq2;
    public ZIPcode__c zip;
    public FamilyComp__c fam;
    Decimal TotalDebt;
    Decimal MonthlyReqIncomeBefTax;
    Decimal ReqAnnuaInBefTaxExcludingMorrHou;
    Decimal EstimatedTaxRate;
    Decimal MonthlyMinPayment;
    Decimal CostofLiving;
    Decimal AnnualCashAvailToPayLoan;
    Decimal MonthCashAvailToPayLoan;
    Decimal TakeHomePayAnnual;
    Decimal TakeHomePayMonth;
    decimal TakeHomePayMonthTwenty;
    Decimal FinalLoanOffer;
    Decimal FinalLoanAmount;
    Decimal FixValue;
    
    
    
    public RulePart(String AppID){
        ApID=AppID;
    }
  
  
    public Boolean Calcuate(){
    getapplication();
        boolean Prebureau=false;
           if(app.genesis__contact__r.age__c>18  &&  app.genesis__contact__r.Length_of_Employment__c>1  &&  app.genesis__contact__r.Income__c>30000){
           
			Prebureau=true; 
            system.debug('11'+Prebureau);
        }
        system.debug('12'+Prebureau);
        return Prebureau;
    }
    
    public  void Equifax(integer Revo,integer open,integer insta,integer mort,integer lineof,integer other,string apid){
     
        
        try{
         // getEquifax();
           eq1 =[select id,Application__c,Revolving__c,Line_of_CR__c,Open__c,Installment__c,Other__c,Mortgage__c,TotalDebt__c
                   from Equifax__c where Application__c=:ApID limit 1];
            
        }catch(QueryException e){
            system.debug('Exeption');
        }
      
        if(eq1==null){
            eq=new Equifax__c();
            eq.Revolving__c=Revo;
       	    eq.Line_of_CR__c=lineof;
            eq.Open__c= open;
        	eq.Installment__c=insta;
        	eq.Mortgage__c=mort;
        	eq.Other__c=other;
        	eq.Application__c=ApID;
            insert eq;
        
        }
        else{
            
            eq1.Revolving__c=Revo;
       	    eq1.Line_of_CR__c=lineof;
            eq1.Open__c= open;
        	eq1.Installment__c=insta;
        	eq1.Mortgage__c=mort;
        	eq1.Other__c=other;
        	eq1.Application__c=ApID;
            update eq1;
           
           
        }
      
    }
    
    public void mit(){
      // getapplication();
        getequifax();
   zip=[select id,county__r.County_Name__c,ZIPcodename__c from ZIPcode__c where ZIPcodename__c=:app.genesis__contact__r.MailingPostalcode ];
        system.debug('+++++++'+zip.county__r.County_Name__c);
        
        fam=[select id,	Annual_Taxes__c,CountyName__c,Housing_Expenses__c,No_of_Dependents__c,Required_Annual_Income_before_taxes__c,Spouse_Earning__c from FamilyComp__c where CountyName__r.County_Name__c=:zip.county__r.County_Name__c and Spouse_Earning__c=:app.genesis__contact__r.Is_your_Spouse_Earning__c and No_of_Dependents__c=:app.genesis__contact__r.Dependents__c];
        system.debug('++++++++=============='+fam);
       
        
        // Calculatoins
        
	MonthlyReqIncomeBefTax=fam.Required_Annual_Income_before_taxes__c/12;
	// System.debug('Required_Annual_Income_before_taxes__c'+Monthly_Req_Income_Bef_Tax);
	ReqAnnuaInBefTaxExcludingMorrHou=fam.Required_Annual_Income_before_taxes__c+eq2.Mortgage__c-fam.Housing_Expenses__c;
        System.debug('Req_annua_in_bef_tax_excluding_morr_hou|||||||||||'+ReqAnnuaInBefTaxExcludingMorrHou);
        
        EstimatedTaxRate=fam.Annual_Taxes__c/ReqAnnuaInBefTaxExcludingMorrHou;
        system.debug('EstimatedTaxRate'+EstimatedTaxRate);
        
        MonthlyMinPayment=eq2.TotalDebt__c+(ReqAnnuaInBefTaxExcludingMorrHou/12);
        system.debug('MonthlyMinPayment'+MonthlyMinPayment);
        
        CostofLiving=MonthlyMinPayment*12*(1+ EstimatedTaxRate);
        system.debug('CostofLiving'+CostofLiving);
        
        AnnualCashAvailToPayLoan=app.genesis__contact__r.Income__c-CostofLiving;
        system.debug('AnnualCashAvailToPayLoan'+AnnualCashAvailToPayLoan);
        
        MonthCashAvailToPayLoan=AnnualCashAvailToPayLoan/12;
        system.debug('MonthCashAvailToPayLoan___________________'+MonthCashAvailToPayLoan);
        
        if(app.genesis__contact__r.FileFromEmpFIle__c==false){
             TakeHomePayAnnual= app.genesis__contact__r.Income__c-(app.genesis__contact__r.Income__c*0.315);
           
            system.debug('TakeHomePayAnnual'+TakeHomePayAnnual);
            
        }
        
        else{
            TakeHomePayAnnual=app.genesis__contact__r.Income__c;
            system.debug('TakeHomePayAnnual________'+TakeHomePayAnnual);
            system.debug('app.genesis__contact__r.Income__c'+app.genesis__contact__r.FileFromEmpFIle__c);
        }
        
        TakeHomePayMonth=TakeHomePayAnnual/12;
        system.debug('TakeHomePayMonth________'+TakeHomePayMonth);
        
        TakeHomePayMonthTwenty=TakeHomePayMonth*0.2;
        system.debug('TakeHomePayMonthTwenty'+TakeHomePayMonthTwenty);
        
        if(TakeHomePayMonthTwenty>=MonthCashAvailToPayLoan){
            FinalLoanOffer=MonthCashAvailToPayLoan;
            system.debug('FinalLoanOffer'+FinalLoanOffer);
        }
        else{
           FinalLoanOffer=TakeHomePayMonthTwenty;
            system.debug('FinalLoanOfferelse'+FinalLoanOffer);
        }
        
        if(app.genesis__Payment_Frequency__c=='monthly'){
           // PrinciAmount=
                }
        //FinalLoanAmount=FinalLoanOffer*12;
       FinalLoanAmount=((((FinalLoanOffer*app.genesis__Term__c)+50)/100).setscale(0)*100);
            system.debug('FinalLoanAmount'+FinalLoanAmount);
        if(FinalLoanAmount>=app.genesis__Loan_Amount__c){
            FixValue=app.genesis__Loan_Amount__c;
        }
        else{
            FixValue=FinalLoanAmount;
        }
      }
    
    public void createPricing(){
        
        genesis__Application_Pricing_Detail__c pric= new genesis__Application_Pricing_Detail__c();
        		pric.genesis__Application__c=apID;
                pric.genesis__Interest_Rate__c= app.genesis__Interest_Rate__c;
                pric.genesis__Payment_Frequency__c= app.genesis__Payment_Frequency__c;
                pric.genesis__Maximum_Financed_Amount__c=FixValue;
                pric.genesis__Term__c= app.genesis__Term__c;
                pric.genesis__Yield_Percent__c=10;
                pric.genesis__Rate_Factor__c=30;
                pric.genesis__Rate_Factor_Based_On_Amount__c=200;
        try{
            insert pric;
        }
        Catch(exception e){
            system.debug(e.getMessage());
        }
    }
    
     
    public  genesis__Applications__c getapplication(){
        
        App=[select genesis__Interest_Rate__c,genesis__Loan_Amount__c,genesis__Payment_Frequency__c,genesis__Term__c,genesis__contact__c,genesis__contact__r.Age__c,genesis__contact__r.Length_of_Employment__c,genesis__contact__r.Income__c,genesis__contact__r.Is_your_Spouse_Earning__c,genesis__contact__r.MailingPostalcode,genesis__contact__r.Dependents__c,genesis__contact__r.FileFromEmpFIle__c from genesis__Applications__c where Id =:ApID];
    return app;
    }
     public  equifax__c getequifax(){
        
       eq2= [select id,Application__c,Revolving__c,Line_of_CR__c,Open__c,Installment__c,Other__c,Mortgage__c,TotalDebt__c
                   from Equifax__c where Application__c=:ApID limit 1];
			return eq2;
    }
    
    
    
    
}