public class FileUploader 
{
   
    public string fileName{get;set;}
    public Blob contentFile{get;set;}   
    
    /***This function reads the CSV file ***/
    public Pagereference ReadFile()
    {
        try{
         
           if(fileName!=null && contentFile!=null){
            
            loan__Org_Parameters__c orgPar= loan.CustomSettingsUtil.getOrgParameters();
            Set<String> sfFileNames = new Set<String>();
            String originalFileName='';
            String sfFileName;
            string tempTimeStamp;
            string timeStamp;
            List<string> splitArray= new List<string>();
            sfFileName=fileName.removeEnd('.csv');
            originalFileName=sfFileName;
            sfFileNames.add(sfFileName);
            tempTimeStamp=sfFileName.right(8);
            sfFileName=sfFileName.removeEnd(tempTimeStamp);
            timeStamp=tempTimeStamp.replaceAll('-',':');
            sfFileName=sfFileName+timeStamp;
            sfFileNames.add(sfFileName);
            sfFileName=sfFileName.removeEnd(timeStamp);
            timeStamp=tempTimeStamp.replaceAll('_',':');
            sfFileName=sfFileName+timeStamp;
            sfFileNames.add(sfFileName);            
            system.debug('****sfFileNames'+sfFileNames);
            
            List<Document> deductFile=[select id,body from document where name IN:sfFileNames limit 1];
            if(deductFile.size()>0){
                    
                    document d=deductFile.get(0);                    
                    String receivedBody= getStringBody(contentFile);
                    String deductBody  = getStringBody(d.body);                             
                    List<List<String>> receivedFileDetails= parseCSV(receivedBody,false);
                    List<List<String>> deductFileDetails= parseCSV(deductBody,false);   
                    Map<string,string> receivedFielMap= new Map<string,string>();
                    Map<string,string> sentFielMap= new Map<string,string>();
                    Map<string,Decimal> billNoToAmountDetail = new Map<string,Decimal>();
                    Map<string,loan__Loan_account_Due_Details__c> billNoToBillDetail = new Map<string,loan__Loan_account_Due_Details__c>();
                    string recField=orgPar.Reconcile_Column_Name__c;                    
                    string repaymentAmount=orgPar.Repayment_Amount_Column_Name__c;
                    string receivedAmount=orgPar.Received_Amount_Column_Name__c;
                    //Exception_Files_Folder_Name__c
                    integer recReceivedHeaderNum;
                    integer receivedColNum;
                    integer recSentHeaderNum;
                    integer deductionColNum;
                    
                    
                    
                    
                    if(receivedFileDetails.size()>0){
                        
                        for(integer i=0;i<receivedFileDetails[0].size();i++){
                            
                            string value=receivedFileDetails[0].get(i);
                            if(value.trim()==recField){
                                recReceivedHeaderNum=i;
                            }                           
                            else if(value.trim()==receivedAmount){
                                receivedColNum=i;
                            }
                            
                        }
                    }
                    if(deductFileDetails.size()>0){
                        
                        for(integer i=0;i<deductFileDetails[0].size();i++){
                            
                            string value=deductFileDetails[0].get(i);
                            if(value.trim()==recField){
                                recSentHeaderNum=i;
                            }                           
                            else if(value.trim()==repaymentAmount){
                                deductionColNum=i;
                            }
                            
                        }
                    }
                   
                    if(recReceivedHeaderNum!=null && receivedColNum!=null){
                        
                            for(integer i=1;i<receivedFileDetails.size();i++){
                                
                                  system.debug('****receivedFileDetails[i]'+receivedFileDetails[i]);
                                  try{
                                    receivedFielMap.put(receivedFileDetails[i].get(recReceivedHeaderNum).trim(),receivedFileDetails[i].get(receivedColNum).trim());
                                  }
                                  catch(exception e){
                                       system.debug('*****Exception in getting received column value : '+e.getmessage());
                                  }
                            }
                    
                    }
                    if(recSentHeaderNum!=null && deductionColNum!=null){
                        
                            for(integer i=1;i<deductFileDetails.size();i++){
                                    try{
                                         system.debug('****deductFileDetails[i]'+deductFileDetails[i]);
                                    sentFielMap.put(deductFileDetails[i].get(recSentHeaderNum).trim(),deductFileDetails[i].get(deductionColNum).trim());
                                    }
                                  catch(exception e){
                                       system.debug('*****Exception in getting sent file column value : '+e.getmessage());
                                  }
                            }
                    
                    }
                    Id folderId;
                    List<Folder> folders  =[select id from Folder where name =:orgPar.Exception_Files_Folder_Name__c limit 1];
                    string excelHeader = orgPar.Reconcile_Column_Name__c+','+orgPar.Repayment_Amount_Column_Name__c+','+orgPar.Received_Amount_Column_Name__c+',Comment'+'\n';
                    String exceptionValues='';
                    if(folders.size()>0){
                        folderId=folders.get(0).id;
                    }
                    else{
                        folderId=UserInfo.getUserId();
                    }
                    
                   
                    for(String row : sentFielMap.keySet()){
                        
                            if(receivedFielMap.containsKey(row)){
                                
                                if(sentFielMap.get(row)!=receivedFielMap.get(row)){
                                    
                                    exceptionValues+=row+','+sentFielMap.get(row)+','+receivedFielMap.get(row)+',Detail is not matched with Deduction file\n';
                                }
                                else{
                                    
                                    billNoToAmountDetail.put(row,Decimal.ValueOf(sentFielMap.get(row)));
                                }
                                
                                receivedFielMap.remove(row);
                            }
                            else{
                                
                                exceptionValues+=row+','+sentFielMap.get(row)+',Not Found, Detail is not present in Received file\n';
                            }
                        
                        
                    }
                    
                    for(String row : receivedFielMap.keySet()){
                        
                        exceptionValues+=row+',Not Found,'+receivedFielMap.get(row)+',Detail is not present in Sent file\n';
                        
                    }
                    
                    boolean hasLPTToCreate=false;
                    if(!billNoToAmountDetail.isEmpty()){
                        
                        List<mfiflexUtil__Log__c> logLst = new List<mfiflexUtil__Log__c>();
                        List<loan__Loan_account_Due_Details__c> updateBills = new List<loan__Loan_account_Due_Details__c>();
                        List<loan__Loan_account_Due_Details__c> billLst=[Select id,Is_LPT_Created__c,loan__DD_Primary_Flag__c,Name,loan__Due_Date__c,loan__Loan_Account__c from  loan__Loan_account_Due_Details__c
                                                                       where Is_LPT_Created__c=false and name IN:billNoToAmountDetail.keySet()];
                        for(loan__Loan_account_Due_Details__c bill:billLst){
                            
                            billNoToBillDetail.put(bill.name,bill);
                        }
                       
                        
                        for(string billNo : billNoToAmountDetail.keySet()){
                            
                            if(billNoToBillDetail.containsKey(billNo)){
                                loan__Loan_account_Due_Details__c bill=billNoToBillDetail.get(billNo);
                                loan__Loan_Payment_Transaction__c retVal = new loan__Loan_Payment_Transaction__c();         
                                peer.FractionalizationAPI f = peer.FractionalizationAPIFactory.getAPI();
                                try{                                
                                        retVal= f.createLoanPayment(bill.loan__Loan_Account__c,billNoToAmountDetail.get(billNo),bill.loan__Due_Date__c,null);
                                        

                                    if(retVal.id!=null){
                                        hasLPTToCreate=true; 
                                        bill.Is_LPT_Created__c=true;
                                        updateBills.add(bill);
                                    }                   
                                }
                                catch (Exception e) {
                                    system.debug('Exception:::::::::'+e.getMessage()+''+e.getCause()+''+e.getStackTraceString());
                                    mfiflexUtil__Log__c log = new mfiflexUtil__Log__c();
                                    log.name=bill.loan__Loan_Account__c;
                                    log.mfiflexUtil__Message__c = 'Error in creating LPT for Loan Id :'+bill.loan__Loan_Account__c+' Error: '+e.getMessage()+' Line: '+e.getLineNumber();
                                    log.mfiflexUtil__Time__c = System.now();
                                    log.mfiflexUtil__Type__c='Error';
                                    logLst.add(log);    
                                }
                              }                             
                            
                        } 
                        if(updateBills.size()>0){
                            update updateBills;
                        }   
                        if(logLst.size()>0){
                            
                            insert logLst;
                        }                       
                                                                       
                    }                   
                   
                    if(exceptionValues!=''){
                        Document exceptionFile = new Document();
                        exceptionFile.Body=Blob.valueOf(excelHeader+exceptionValues);
                        exceptionFile.ContentType='text/csv';
                        exceptionFile.FolderId=folderId;
                        exceptionFile.Type='csv';
                        exceptionFile.Name='Exception-'+originalFileName;
                        insert exceptionFile;
                        string msg='';
                        if(hasLPTToCreate){
                            msg='Repayment for the loans have been created and File '+exceptionFile.Name+' is saved in folder with exception details.';
                        }
                        else{
                            msg='File '+exceptionFile.Name+' is saved in folder with exception details.';
                        }
                        ApexPages.Message successMsg = new ApexPages.Message(ApexPages.severity.CONFIRM,msg);
                        ApexPages.addMessage(successMsg);
                        
                    }
                    else{
                        
                        string msg='';
                        if(hasLPTToCreate){
                            msg='Deduct file and Received file details are same and Repayment for the loans have been created.';
                        }
                        else{
                            msg='Deduct file and Received file details are same.';
                        }
                       ApexPages.Message successMsg = new ApexPages.Message(ApexPages.severity.CONFIRM,msg);
                       ApexPages.addMessage(successMsg);
                    }
                    
                    
                }
                else{

                    ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Deduct file is not found for this Received file.');
                    ApexPages.addMessage(errormsg);
                }
            
            }
            else{
            
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please upload the Received file.');
                ApexPages.addMessage(errormsg);
            }
            
         }
         
         catch(Exception e){
                 ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured reading the CSV file'+e.getMessage());
                ApexPages.addMessage(errormsg);
         }       
       
        return null;
    }
   
   /**** This function sends back to the visualforce page the list of account records that were inserted ****/ 
    public List<Account> getuploadedAccounts()
    {
       /* if (accstoupload!= NULL)
            if (accstoupload.size() > 0)
                return accstoupload;
            else
                return null;                    
        else
        */
            return null;
    }  
    
    
  // Method to parse the CSV file.  
    
  public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
    List<List<String>> allFields = new List<List<String>>();

    // replace instances where a double quote begins a field containing a comma
    // in this case you get a double quote followed by a doubled double quote
    // do this for beginning and end of a field
    contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
    // now replace all remaining double quotes - we do this so that we can reconstruct
    // fields with commas inside assuming they begin and end with a double quote
    contents = contents.replaceAll('""','DBLQT');
    // we are not attempting to handle fields with a newline inside of them
    // so, split on newline to get the spreadsheet rows
    List<String> lines = new List<String>();
    try {
        lines = contents.split('\n');
    } catch (System.ListException e) {
        System.debug('Limits exceeded?' + e.getMessage());
    }
    Integer num = 0;
    for(String line : lines) {
        // check for blank CSV lines (only commas)
        if (line.replaceAll(',','').trim().length() == 0) break;
        
        List<String> fields = line.split(',');  
        List<String> cleanFields = new List<String>();
        String compositeField;
        Boolean makeCompositeField = false;
        for(String field : fields) {
            if (field.startsWith('"') && field.endsWith('"')) {
                cleanFields.add(field.replaceAll('DBLQT','"'));
            } else if (field.startsWith('"')) {
                makeCompositeField = true;
                compositeField = field;
            } else if (field.endsWith('"')) {
                compositeField += ',' + field;
                cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                makeCompositeField = false;
            } else if (makeCompositeField) {
                compositeField +=  ',' + field;
            } else {
                cleanFields.add(field.replaceAll('DBLQT','"'));
            }
        }
        
        allFields.add(cleanFields);
    }
    if (skipHeaders) allFields.remove(0);
    return allFields;       
}   
 
      public string getStringBody(Blob body){
          
               string csvBody='';
                try{
                    csvBody=body.toString();
                }
                catch(exception e){
                     system.debug('***exception in converting to string'+e.getMessage());
                     csvBody='';
                }
                if(csvBody==''){                      
                  csvBody= blobToString(body,'ISO-8859-1'); 
                }         
          
            return csvBody;
      }
    
        /**
         This function convers the input CSV file in BLOB format into a string
        @param input    Blob data representing correct string in @inCharset encoding
        @param inCharset    encoding of the Blob data (for example 'ISO 8859-1')
     */
    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
       // System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }         
}