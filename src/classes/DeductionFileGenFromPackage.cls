public class DeductionFileGenFromPackage extends loan.FileGenerator{

    Date systemDate;  
    loan.GlobalLoanUtilFacade facade;   
    string fileName='';
    Map<Id,genesis__Employment_Information__c> contToEmpMap;
    List<Deduction_File_Configuration__c> accToDeductLst;
    Boolean isText;
    public DeductionFileGenFromPackage(String name,Boolean isText,Map<Id,genesis__Employment_Information__c> empMap, List<Deduction_File_Configuration__c> accToDeductLst){
        super();      
        facade        = new loan.GlobalLoanUtilFacade();
        systemDate    = facade.getCurrentSystemDate();
        fileName=name;
        contToEmpMap=empMap;
        this.isText=isText;     
        this.accToDeductLst=accToDeductLst;
        system.debug('***contToEmpMap'+contToEmpMap);
        system.debug('****accToDeductLst'+accToDeductLst);
        
        
    }
    
    public override String getSimpleFileName() {
        return fileName;
    }
    
    public override List<String> getEntries(loan.TransactionSweepToACHState state, List<SObject> scope) {
       
        List<String> retVal = new List<String>();
        List<loan__Loan_account_Due_Details__c> dueList = (List<loan__Loan_account_Due_Details__c>)scope;
        List<SObject> objects = new List<SObject>();
        system.debug('****dueList'+dueList);
        for(loan__Loan_account_Due_Details__c due : dueList){
            
            addToValidScope(due);
            //entry Record Detail            
            filegen__Entry_Detail_Record__c entryRecord = new filegen__Entry_Detail_Record__c();
            
                for(Deduction_File_Configuration__c df:accToDeductLst){
                    
                    String fieldValue=getFieldValue(due,df);
                    if(fieldValue==null){
                        fieldValue='';
                    }                   
                    if(isText!=true){
                        fieldValue=fieldValue.remove(',');
                        fieldValue=fieldValue+',';
                    }
                    system.debug('****C'+df.Sequence__c+'__C'+'***Value'+fieldValue);                   
                    entryRecord.put('C'+df.Sequence__c+'__C',fieldValue);                   
                
                }           
            objects.add(entryRecord);   
           
           
        }
        
        filegen.CreateSegments segments = new filegen.CreateSegments(objects);
        retVal = segments.retString();
        system.debug('******retVal'+retVal);
        for(String line : retVal){
            line=line.removeEnd(',');
            line = line+'\n';
            addEntry(line);
            system.debug('******line'+line);
        }
     //   system.debug('******addEntry'+addEntry);
        System.debug(LoggingLevel.ERROR,' Finished Entries ');
        system.debug(LoggingLevel.ERROR,retVal);
        return retVal;
        
             
    }
    
    public override String getHeader(loan.TransactionSweepToACHState state, List<SObject> scope) {
        String header = '';
        List<String> retVal = new List<String>();
        List<SObject> headerRecs = new List<SObject>();
        filegen__File_Header_Record__c headerRec = new filegen__File_Header_Record__c();
                for(Deduction_File_Configuration__c df:accToDeductLst){                 
                    String fieldValue=(df.Header_Label__c!=null ? df.Header_Label__c : '');
                    if(isText!=true){
                        fieldValue=fieldValue.remove(',');
                        fieldValue=fieldValue+',';
                    }                   
                    headerRec.put('C'+df.Sequence__c+'__C',fieldValue);                 
                
                }
            headerRecs.add(headerRec);
            filegen.CreateSegments segments = new filegen.CreateSegments(headerRecs);
            system.debug('segments:::::::::::'+segments);
            retVal = segments.retString();
            system.debug('retval::::::::::::'+retVal+'@@:::::'+segments);
            try{        
             header = retVal[0].removeEnd(',')+'\n';
            }catch(Exception e){
            system.debug(LoggingLevel.ERROR,''+e.getMessage());
            }        
        
        return header;
    }
    
    public override String getTrailer(loan.TransactionSweepToACHState state, LIST<SObject> scope) {
        String trailer = '';
        List<String> retVal = new List<String>();
        
        //trailer of CAIS file
        filegen__File_Control_Record__c trailerRec             = new filegen__File_Control_Record__c();
        
       // return trailer;
       return '';
    }  

    
    public string getFieldValue(loan__Loan_account_Due_Details__c due, Deduction_File_Configuration__c df){
            
            string fieldValue='';
        
               try{
                   system.debug('due'+due);
                    system.debug('****df.Mapped_Object_Name__c'+df.Mapped_Object_Name__c);
                     if(df.Mapped_Object_Name__c=='loan__Loan_account_Due_Details__c'){                      
                           
                           if(df.Field_Type__c!='Date'){
                             fieldValue= string.valueOf(due.get(df.Mapped_Field_API_Name__c));
                           }
                           else{       
                               fieldValue= formatDateField(Date.ValueOf(due.get(df.Mapped_Field_API_Name__c)), (df.Date_Format__c!=null ? df.Date_Format__c : 'YYYY-MM-DD'));
                           }
                     }
                     else if(df.Mapped_Object_Name__c=='loan__Loan_Account__c'){                         
                           
                           if(df.Field_Type__c!='Date'){
                             fieldValue= string.valueOf(due.getSobject('loan__Loan_Account__r').get(df.Mapped_Field_API_Name__c));
                           }
                           else{       
                               fieldValue= formatDateField(Date.ValueOf(due.getSobject('loan__Loan_Account__r').get(df.Mapped_Field_API_Name__c)), (df.Date_Format__c!=null ? df.Date_Format__c : 'YYYY-MM-DD'));
                           }
                     }
                     else if(df.Mapped_Object_Name__c=='Account'){                       
                           
                           if(df.Field_Type__c!='Date'){
                             fieldValue= string.valueOf(due.getSobject('loan__Loan_Account__r').getSobject('loan__Account__r').get(df.Mapped_Field_API_Name__c));
                           }
                           else{       
                               fieldValue= formatDateField(Date.ValueOf(due.getSobject('loan__Loan_Account__r').getSobject('loan__Account__r').get(df.Mapped_Field_API_Name__c)), (df.Date_Format__c!=null ? df.Date_Format__c : 'YYYY-MM-DD'));
                           }
                     }
                     else if(df.Mapped_Object_Name__c=='Contact'){                       
                           
                           if(df.Field_Type__c!='Date'){
                             fieldValue= string.valueOf(due.getSobject('loan__Loan_Account__r').getSobject('loan__Contact__r').get(df.Mapped_Field_API_Name__c));
                           }
                           else{       
                               fieldValue= formatDateField(Date.ValueOf(due.getSobject('loan__Loan_Account__r').getSobject('loan__Contact__r').get(df.Mapped_Field_API_Name__c)), (df.Date_Format__c!=null ? df.Date_Format__c : 'YYYY-MM-DD'));
                           }
                     }
                     else if(df.Mapped_Object_Name__c=='genesis__Employment_Information__c'){                        
                           
                           if(df.Field_Type__c!='Date'){
                             fieldValue= string.valueOf(contToEmpMap.get(due.loan__Loan_Account__r.loan__Contact__c).get(df.Mapped_Field_API_Name__c));
                           }
                           else{       
                               fieldValue= formatDateField(Date.ValueOf(contToEmpMap.get(due.loan__Loan_Account__r.loan__Contact__c).get(df.Mapped_Field_API_Name__c)), (df.Date_Format__c!=null ? df.Date_Format__c : 'YYYY-MM-DD'));
                           }
                     }
                   
                   
               }
               catch(Exception e){
                     fieldValue='';
                    system.debug('****Error in getting field value :'+e.getMessage()+''+e.getCause()+''+e.getLineNumber());
               }
        
              
         return fieldValue;
    }
    
    public String formatDateField(Date s_date,String dateFormat){
          String dateValue='';
        if(s_date != null){
            try{ 
            String s_day, s_month, s_year;
            Integer day = s_date.day();
            Integer month = s_date.month();
            if(day < 10) s_day = '0'+String.valueOf(day);
            else s_day = String.valueOf(day);
            if(month < 10) s_month = '0'+String.valueOf(month);
            else s_month = String.valueOf(month);
            s_year = String.valueOf(s_date.Year());
           // s_year = s_year.substring(2, 4);
            if(dateFormat=='YYYY-MM-DD'){
                dateValue=s_year+'-'+s_month+'-'+s_day;  
            }
            else if(dateFormat=='DD-MM-YYYY'){
                dateValue=s_day+'-'+s_month+'-'+s_year;  
            }
            else if(dateFormat=='MM-DD-YYYY'){
                dateValue=s_month+'-'+s_day+'-'+s_year;  
            }
           
            }
            catch(exception e){
                    dateValue='';
                    system.debug('****Error in getting field value :'+e.getMessage()+''+e.getCause()+''+e.getLineNumber());
            }           
            return dateValue;
              
        }
        else {
            
            return dateValue;
        }        
    } 
}