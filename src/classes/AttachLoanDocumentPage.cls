public class AttachLoanDocumentPage {
    
    public String loanid{get;set;}
    Private static loan__Loan_Account__c pageOpp;
   
    public AttachLoanDocumentPage(ApexPages.StandardController stdController) {
      
        pageOpp = (loan__Loan_Account__c)stdController.getRecord();
        loanid = pageOpp.id;
  
    }
   // @RemoteAction
    @Future(callout=true)
    public static void  method1(string loaid)
    {
        
       	try 
    	{
            pageReference pagePdf = Page.LoanDocumentPage;
            pagePdf.getParameters().put('id', String.valueof(loaid));
            Blob pdfPageBlob;
            pdfPageBlob = pagePdf.getContentAsPDF(); 
            system.debug('11111111111'+pdfPageBlob);
            Attachment a = new Attachment();
            a.ContentType='application/pdf';
            a.Body = pdfPageBlob;
            a.ParentID = loaid; 
            a.Name = 'BorrowerAgreement.pdf'; 
            a.Description = 'TestDescription1'; 
             system.debug('11111111111'+a);
            insert a;  
            system.debug('save'+pagePdf);
        	//return true;
    	}
         catch (Exception e) 
         {
        	 //body = Blob.valueOf('Some Text');
             system.debug('the error is'+e.getMessage()+'at Line number'+e.getLineNumber());
           //  return false;
    	 }
        
    }

}