trigger PricingCreation on genesis__Applications__c (After insert,After update) {

    if((Trigger.isAfter && Trigger.isInsert) ) {
 //   TriggerHandlerForCreatePricing.CreatingPricing(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.isUpdate){
    system.debug('inside trigger');
    Set<id> appSet = new Set<id>();
       for(genesis__Applications__c app:Trigger.New){
       if(app.genesis__Status__c =='APPROVED - APPROVED'){
       appSet.add(app.id);
       }
       }
       if(appSet.size()>0){
       TriggerHandlerForCreatePricing.SendSms(appSet);
       }
    }
}