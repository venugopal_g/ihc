trigger LoanAttachmentTrigger on loan__Loan_Account__c (after insert)
{
    if((Trigger.isAfter && Trigger.isInsert) ){
         
         for(loan__Loan_Account__c loan : Trigger.New)
        {
           system.debug('Loan Id** '+loan.Id);
           AttachLoanDocumentPage.method1(loan.Id);
            

        } 
      
    }
    
}