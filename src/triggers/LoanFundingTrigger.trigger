trigger LoanFundingTrigger on loan__Loan_Disbursal_Transaction__c (before Insert,after Insert) {
if(trigger.isInsert && trigger.isAfter){
  LoanFundingTriggerHandler.SendSms(trigger.newMap.keyset());
}
}